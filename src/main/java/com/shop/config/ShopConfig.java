package com.shop.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.shop.ShopService;
import com.swak.annotation.Flux;

/**
 * 测试配置
 * 
 * @author lifeng
 */
@Configuration
public class ShopConfig {

	@Bean
	@Flux(ShopService.class)
	public ShopService shopService() {
		return new ShopService();
	}
}