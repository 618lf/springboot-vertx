package com.shop;

import com.swak.annotation.FluxAsync;
import com.swak.annotation.FluxService;

@FluxAsync
@FluxService
public class ShopService {

	/**
	 * 非异步方法，直接调用
	 * 
	 * @return
	 */
	public String get() {
		return "I am Shop";
	}
}