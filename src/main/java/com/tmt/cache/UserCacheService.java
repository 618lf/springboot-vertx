package com.tmt.cache;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import org.springframework.beans.factory.annotation.Autowired;

import com.swak.annotation.Context;
import com.swak.annotation.FluxReference;
import com.swak.annotation.FluxService;
import com.swak.lock.AsyncLock.AsyncLockItem;
import com.swak.redis.RedisService;
import com.tmt.api.entity.User;
import com.tmt.api.service.UserServiceAsync;

/**
 * 用户缓存服务
 * 
 * @author lifeng
 * @date 2020年8月19日 下午12:06:41
 */
@FluxService(context = Context.IO)
public class UserCacheService {

	@Autowired
	private CacheService cacheService;
	@FluxReference
	private UserServiceAsync userService;
	@Autowired(required = false)
	private RedisService redisService;

	/**
	 * 异步获取缓存
	 * 
	 * @param account
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public CompletionStage<User> getByAccount(String account) {
		// 先从缓存中获取
		return cacheService.getUserCache().getObject(account).thenCompose(user -> {
			if (user == null) {

				// 持有唯一号的全局锁，可以使用此号多次重入
				AsyncLockItem redisLock = redisService.getAsyncLock("user").newLockItem();

				// 获取锁资源之后去查询数据，并释放锁，如果获取数据的时间比较长，则会刷新过期时间
				return redisLock.doHandler(() -> {
					return this.getUser(account);
				});
			}
			return CompletableFuture.completedFuture(user);
		});
	}

	/**
	 * 实际的重数据库取数据
	 * 
	 * @param account
	 * @return
	 */
	private CompletableFuture<User> getUser(String account) {
		return userService.getByAccount(account);
	}
}