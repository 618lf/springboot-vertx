package com.tmt.cache;

import java.util.concurrent.CompletionStage;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.swak.annotation.Context;
import com.swak.annotation.FluxService;
import com.swak.cache.AsyncCache;
import com.swak.cache.Cache;
import com.swak.cache.CacheManager;
import com.tmt.api.entity.User;/**
 * 缓存服务
 * 
 * @author lifeng
 * @date 2020年4月14日 下午10:16:03
 */
@FluxService(context = Context.IO)
public class CacheService implements InitializingBean {

	/**
	 * 缓存管理器 -- 由于本地没有配置 redis
	 */
	@Autowired(required = false)
	private CacheManager cacheManager;
	
	/**
	 * 授权 token 的存储(公众号登录)
	 */
	private AsyncCache<String> tokenCache;
	

	/**
	 * 用户 的存储
	 */
	private AsyncCache<User> userCache;

	@Override
	public void afterPropertiesSet() throws Exception {

		/**
		 * 为演示需要加入了缓存
		 */
		if (cacheManager == null) {
			return;
		}

		Cache<String> tokenCache = cacheManager.getCache("token", 60 * 5, false);
		this.tokenCache = tokenCache.async();

		Cache<User> userCache = cacheManager.getCache("user", 60 * 5, false);
		this.userCache = userCache.async();
		
		
	}

	public AsyncCache<String> getTokenCache() {
		return tokenCache;
	}

	public AsyncCache<User> getUserCache() {
		return userCache;
	}

	/**
	 * 存储一个token
	 * 
	 * @param key
	 * @param token
	 * @return
	 */
	public CompletionStage<String> addToken(String key, String token) {
		return tokenCache.putString(key, token).thenApply(ress -> key);
	}

	/**
	 * 获得存储的token
	 * 
	 * @param key
	 * @param token
	 * @return
	 */
	public CompletionStage<String> getToken(String key) {
		return tokenCache.getStringAndDel(key);
	}

}
