package com.tmt;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.swak.Application;
import com.swak.loader.PriorityClassLoader;

/**
 * 系统启动
 * 
 * @author: lifeng
 * @Date: 2020/3/28 11:47
 */
@SpringBootApplication
public class AppRunner {

	/**
	 * 可以定义类加载器
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Thread.currentThread().setContextClassLoader(new PriorityClassLoader());
		Application.run(AppRunner.class, args);
	}
}