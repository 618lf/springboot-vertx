package com.tmt.realm;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import com.swak.security.AuthorizationInfo;
import com.swak.security.Subject;
import com.swak.vertx.security.realm.Realm;

/**
 * 用户权限获取
 * 
 * @author lifeng
 * @date 2020年4月8日 下午9:03:26
 */
public class UserRealm implements Realm {

	/**
	 * 动态的设置权限
	 */
	@Override
	public CompletionStage<AuthorizationInfo> doGetAuthorizationInfo(Subject subject) {
		AuthorizationInfo info = new AuthorizationInfo();
		info.addRole("admin").addPermission("admin:add");
		return CompletableFuture.completedFuture(info);
	}
}