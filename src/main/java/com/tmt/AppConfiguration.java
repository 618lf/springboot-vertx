package com.tmt;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.shop.config.ShopConfig;
import com.swak.App;
import com.swak.annotation.ImOps;
import com.swak.config.vertx.SecurityConfigurationSupport;
import com.swak.security.JWTAuthOptions;
import com.swak.security.options.KeyStoreOptions;
import com.swak.vertx.config.ImConfig;
import com.swak.vertx.config.RouterConfig;
import com.swak.vertx.security.SecurityHandler;
import com.swak.vertx.transport.VertxProxy;
import com.tmt.realm.UserRealm;

/**
 * 系统使用资源配置
 * 
 * @author: lifeng
 * @Date: 2020/3/28 11:48
 */
@Configuration
@Import({ ShopConfig.class })
public class AppConfiguration {

	/**
	 * 用户域对象
	 * 
	 * @return
	 */
	@Bean
	public UserRealm userRealm() {
		return new UserRealm();
	}

	/**
	 * 安全过滤
	 *
	 * @return SecurityConfigurationSupport
	 */
	@Bean
	public SecurityConfigurationSupport securitySupport(UserRealm userRealm) {

		// #### 第一种配置方式，编程式的配置方式，这种配置方式优先级最高 ####

		// 认证授权配置（可以配置多种认证授权算法）
		JWTAuthOptions jwtAuthOptions = new JWTAuthOptions();
		jwtAuthOptions.setKeyStore(new KeyStoreOptions().setPath("keystore.jceks") // 查找classes根目录keystore.jceks文件
				.setPassword("secret"));

		// 默认使用的是HS256，如果需要修改可以自定义JWTOptions此算法需要和keystore.jceks设置的一致
		// jwtAuthOptions.getJWTOptions().setAlgorithm(algorithm);

		// #### 第二种配置方式，直接在配置文件中配置 #####

		// spring.vertx.keyStorePath=(如果不填写会查找classes根目录下的文件)
		// spring.vertx.keyStorePass=
		// spring.vertx.keyStoreAlgorithm=(默认是HS256)

		// #### 第三种配置方式，根据需要自由发挥 #####

		// 权限配置
		SecurityConfigurationSupport support = new SecurityConfigurationSupport();
		support.definition("/api/anno=anno").definition("/api/user/wechat=anno").definition("/api/user/=user")
				.definition("/api/admin/=user, role[admin]").definition("/=anno").setRealm(userRealm)
				.setJwtAuthOptions(jwtAuthOptions);
		return support;
	}

	/**
	 * 配置Http 的通用处理
	 *
	 * @return RouterConfig
	 */
	@Bean
	public RouterConfig routerConfig(App app, VertxProxy proxy, SecurityHandler security) {
		return RouterConfig.defConfig(app, proxy, security);
	}

	/**
	 * 配置Im 的 通用处理
	 *
	 * @return ImConfig
	 */
	@Bean
	public ImConfig imRouterConfig(SecurityHandler handler) {
		return (vertx, router) -> {
			router.route().ops(ImOps.Connect).handler(handler.imHandler());
		};
	}
}