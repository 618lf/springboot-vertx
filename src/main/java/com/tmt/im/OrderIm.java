package com.tmt.im;

import java.util.List;

import com.swak.annotation.ImApi;
import com.swak.annotation.ImMapping;
import com.swak.annotation.ImOps;
import com.swak.security.Subject;
import com.swak.utils.Lists;
import com.swak.vertx.protocol.im.ImContext;
import com.swak.vertx.protocol.im.ImSocket;

/**
 * 定义IM -- 用 id 接收订单ID
 * <p>
 * 如果域名和端口一致cookie是可以传递到ws这端的
 * <p>
 * 
 * @author lifeng
 * @date 2020年8月26日 下午11:34:47
 */
@ImApi(value = "orderIm", path = "/order/:id")
public class OrderIm {

	List<ImSocket> sockets = Lists.newArrayList();

	@ImMapping(method = ImOps.Connect)
	public void imConnect(ImContext context, ImSocket socket2, String id, Subject subject) {
		System.out.println("连接:" + context.request().uri() + "参数：" + id);
		ImSocket socket = context.socket();
		sockets.add(socket);
	}

	@ImMapping(method = ImOps.Message)
	public void imMessage(ImContext context, String id) {
		System.out.println("消息:" + context.request().uri() + "参数：" + id);
	}
}