package com.tmt.api.web;

import java.util.concurrent.CompletionStage;

import com.swak.annotation.FluxReference;
import com.swak.annotation.Post;
import com.swak.annotation.RestApi;
import com.swak.entity.Result;
import com.swak.security.Subject;
import com.tmt.api.entity.UserAccount;
import com.tmt.api.service.UserServiceAsync;

import io.vertx.ext.web.RoutingContext;

/**
 * 系统注册
 * 
 * @author lifeng
 * @date 2020年4月14日 下午7:39:37
 */
@RestApi(path = "/api/register", value = "registerApi")
public class RegisterApi {

	/**
	 * 异步的用户服务
	 */
	@FluxReference
	private UserServiceAsync userService;

	/**
	 * 通过： UserName 和 Password 来登陆系统 <br>
	 * 
	 * @param subject 主体
	 * @param context 请求上下文
	 * @return 注册结果
	 */
	@Post("/account")
	public CompletionStage<Result> registerAccount(String userName, String password, Subject subject,
			RoutingContext context) {
		UserAccount account = new UserAccount();
		account.setName(userName);
		account.setPassword(password);
		return userService.register(account).thenApply(Result::success);
	}

}
