package com.tmt.api.web;

import java.util.concurrent.CompletableFuture;

import org.springframework.beans.factory.annotation.Autowired;

import com.swak.annotation.FluxReference;
import com.swak.annotation.Post;
import com.swak.annotation.RestApi;
import com.swak.entity.Result;
import com.swak.security.Subject;
import com.swak.vertx.transport.VertxProxy;
import com.tmt.api.service.UserServiceAsync;

import io.vertx.core.http.Cookie;
import io.vertx.ext.web.RoutingContext;

/**
 * 用户API
 * 
 * @author: lifeng
 * @Date: 2020/3/28 11:50
 */
@RestApi(path = "/api/user", value = "userApi")
public class UserApi {

	/**
	 * 使用异步服务
	 */
	@FluxReference
	private UserServiceAsync userService;
	@Autowired
	private VertxProxy proxy;

	/**
	 * 获得用户
	 * 
	 * @Date 2020/3/28 12:06
	 * @Param subject 用户信息
	 * @Return 异步结果
	 */
	@Post("/get")
	public CompletableFuture<Result> get(Subject subject) {
		return userService.get(subject.getPrincipal().getIdAsLong()).thenApply(Result::success);
	}

	/**
	 * 授权登陆
	 * 
	 * @Date 2020/3/28 12:06
	 * @Param subject 用户信息
	 * @Return 异步结果
	 */
	@Post("/wechat/oauth")
	public CompletableFuture<Result> oauth(RoutingContext context, Subject subject) {
		context.addCookie(Cookie.cookie(proxy.config().getTokenName(), "xxxx").setPath("/").setHttpOnly(true));
		return CompletableFuture.completedFuture(Result.success("xxx"));
	}
}