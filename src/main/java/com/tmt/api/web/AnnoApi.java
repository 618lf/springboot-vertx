package com.tmt.api.web;

import java.util.concurrent.CompletableFuture;

import com.swak.annotation.Get;
import com.swak.annotation.RestApi;
import com.swak.entity.Result;
import com.swak.utils.Lists;
import com.tmt.api.entity.MultiOrder;
import com.tmt.api.entity.OrderItem;
import com.tmt.api.entity.OrderItemItem;
import com.tmt.api.entity.OrderItemItemItem;

/**
 * @author: lifeng
 * @Date: 2020/3/28 11:50
 * @Description:
 */
@RestApi(path = "/api/anno", value = "annoApi")
public class AnnoApi {

	/**
	 * 获取用户
	 *
	 * @return 异步接口
	 */
	@Get("/get")
	public CompletableFuture<Result> get() {
		System.out.println("old version");
		return CompletableFuture.completedFuture(Result.success());
	}

	/**
	 * 获取用户
	 *
	 * @return 异步接口
	 */
	@Get("/json")
	public CompletableFuture<Result> json() {
		MultiOrder order = new MultiOrder();
		OrderItem orderItem = new OrderItem();
		OrderItemItem orderItemItem = new OrderItemItem();
		OrderItemItemItem orderItemItemItem = new OrderItemItemItem();
		OrderItemItemItem other = new OrderItemItemItem();
		orderItemItemItem.setId("1");
		orderItemItemItem.setName("2");
		other.setId("11");
		other.setName("22");
		order.setItems(Lists.newArrayList(orderItem));
		orderItem.setItems(Lists.newArrayList(orderItemItem));
		orderItemItem.setItems(Lists.newArrayList(orderItemItemItem));
		orderItemItemItem.setItems(Lists.newArrayList(other));
		return CompletableFuture.completedFuture(Result.success(order));
	}
}
