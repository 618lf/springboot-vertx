package com.tmt.api.web;

import java.util.concurrent.CompletableFuture;

import com.swak.annotation.FluxReference;
import com.swak.annotation.Get;
import com.swak.annotation.RequiresPermissions;
import com.swak.annotation.RequiresRoles;
import com.swak.annotation.RestApi;
import com.swak.entity.Result;
import com.swak.security.Subject;
import com.tmt.api.service.UserServiceAsync;

/**
 * @author: lifeng
 * @Date: 2020/3/28 11:49
 * @Description:
 */
@RestApi(path = "/api/admin", value = "adminApi")
public class AdminApi {

	@FluxReference
	private UserServiceAsync userService;

	/**
	 * 获取用户
	 *
	 * @param subject 用户
	 * @return 结果
	 */
	@Get("/get")
	public CompletableFuture<Result> get(Subject subject) {
		return userService.get(subject.getPrincipal().getIdAsLong()).thenApply(Result::success);
	}

	/**
	 * 获取用户
	 *
	 * @param subject 用户
	 * @return 异步结果
	 */
	@RequiresRoles({ "admin:role1", "admin:role2" })
	@Get("/get_role")
	public CompletableFuture<Result> getRole(Subject subject) {
		return userService.get(subject.getPrincipal().getIdAsLong()).thenApply(Result::success);
	}

	/**
	 * 获取用户
	 *
	 * @param subject 用户
	 * @return 异步结果
	 */
	@RequiresPermissions({ "admin:role1:op1", "admin:role2:op2" })
	@Get("/get_permission")
	public CompletableFuture<Result> getPermission(Subject subject) {
		return userService.get(subject.getPrincipal().getIdAsLong()).thenApply(Result::success);
	}
}
