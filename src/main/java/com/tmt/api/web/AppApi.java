package com.tmt.api.web;

import java.util.concurrent.CompletableFuture;

import com.swak.Application;
import com.swak.annotation.Get;
import com.swak.annotation.RestApi;

/**
 * App 服务
 * 
 * @author lifeng
 * @date 2020年10月26日 下午8:56:46
 */
@RestApi(path = "/api/app", value = "appApi")
public class AppApi {

	/**
	 * 停止服务
	 */
	@Get("/shutdown")
	public void shutdown() {
		CompletableFuture.runAsync(() -> {
			Application.stop();
		});
	}
}