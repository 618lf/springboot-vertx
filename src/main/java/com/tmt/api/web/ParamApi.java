package com.tmt.api.web;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import org.springframework.core.io.Resource;

import com.swak.App;
import com.swak.annotation.Body;
import com.swak.annotation.Get;
import com.swak.annotation.Header;
import com.swak.annotation.Json;
import com.swak.annotation.Post;
import com.swak.annotation.RestApi;
import com.swak.annotation.Valid;
import com.swak.entity.Model;
import com.swak.entity.Result;
import com.swak.utils.Maps;
import com.swak.validator.errors.BindErrors;
import com.swak.vertx.transport.multipart.PlainFile;
import com.swak.vertx.transport.multipart.Response;
import com.tmt.api.dto.Param;

import io.vertx.ext.web.RoutingContext;

/**
 * @author: lifeng
 * @Date: 2020/3/28 11:50
 * @Description:
 */
@RestApi(path = "/api/param")
public class ParamApi {

	/**
	 * 输出string 数据
	 *
	 * @return String
	 */
	@Get("/string")
	public String string() {
		return "Hello World, Lifeng!";
	}

	/**
	 * 输出string 数据
	 *
	 * @return String
	 */
	@Get("/string/:id")
	public String string(String id) {
		return "Hello World! " + id;
	}

	/**
	 * 输出string 数据
	 *
	 * @return String
	 */
	@Get("/string_get_param/:id")
	public String string(String id, String name) {
		return "Hello World! " + id + ":name=" + name;
	}

	/**
	 * 输出string 数据
	 *
	 * @return String
	 */
	@Post("/post_param")
	public String string(Integer p1, String p2, List<String> p3, Map<String, Object> p4) {
		return "Hello World! p1=" + p1 + ":p2=" + p2 + ":p3=" + p3 + ":p4=" + p4;
	}

	/**
	 * 输出string 数据
	 *
	 * @return String
	 */
	@Post("/post_param_obj")
	public String string(Param param) {
		return "Hello World! p1=" + param.getP1() + ":p2=" + param.getP2() + ":p3=" + param.getP3() + ":p4="
				+ param.getP4() + ":oneItem=" + param.getOneItem() + ":items" + param.getItems();
	}

	/**
	 * 输出string 数据
	 *
	 * @return String
	 */
	@Post("/post_param_obj_mutil")
	public String postParamObjMutil(Param param) {
		return "Hello World! p1=" + param.getP1() + ":p2=" + param.getP2() + ":p3=" + param.getP3() + ":p4="
				+ param.getP4() + ":oneItem=" + param.getOneItem() + ":items" + param.getItems();
	}

	/**
	 * 输出string 数据
	 *
	 * @return String
	 */
	@Post("/post_param_anno")
	public String string(@Json Map<String, Object> json, @Body byte[] bytes, @Header String name) {
		return "Hello World! json=" + json + ";body=" + Arrays.toString(bytes) + ";header[name]=" + name;
	}

	/**
	 * 输出string 数据
	 *
	 * @return Result
	 */
	@Get("/json")
	public Result json() {
		return Result.success("lifre");
	}

	/**
	 * 输出string 数据
	 *
	 * @return Result
	 */
	@Get("/xml")
	public Param xml() {
		Param param = new Param();
		param.setP1(1);
		return param;
	}

	/**
	 * 通过 zero copy 的方式输出文本
	 *
	 * @return Result
	 * @throws IOException yichang
	 */
	@Get("/zerocopy")
	public CompletableFuture<PlainFile> zeroCopy() throws IOException {
		Resource resource = App.resource("/com/tmt/api/web/bigtext.txt");
		File file = resource.getFile();
		return CompletableFuture.completedFuture(PlainFile.of(file));
	}

	/**
	 * 通过 模板输出数据
	 *
	 * @return html
	 */
	@Get("/html")
	public CompletableFuture<Model> html() {
		return CompletableFuture.completedFuture(Model.use("index.html").addAttribute("name", "lifeng"));
	}

	/**
	 * 输出string 数据
	 *
	 * @return String
	 */
	@Post("/valid")
	public String valid(@Valid Param param, BindErrors errors) {
		return "Hello World! p1=" + param.getP1() + ":p2=" + param.getP2() + ":p3=" + param.getP3() + ":p4="
				+ param.getP4() + ":Errors=" + errors.getErrors();
	}

	/**
	 * 输出string 数据
	 *
	 * @return String
	 */
	@Get("/all_param")
	public Response all_get_param(RoutingContext context) {
		System.out.println("GET 请求参数：" + context.request().params());
		Map<String, String> data = Maps.newHashMap();
		data.put("data", "ok");
		return Response.json(data);
	}

	/**
	 * 输出string 数据
	 *
	 * @return String
	 */
	@Post("/all_param")
	public Response all_post_param(RoutingContext context) {
		System.out.println("POST 请求参数：" + context.request().params());
		System.out.println("POST 请求Body：" + context.getBodyAsString());
		Map<String, String> data = Maps.newHashMap();
		data.put("data", "ok");
		return Response.json(data);
	}
}