package com.tmt.api.web;

import java.util.concurrent.CompletableFuture;

import com.swak.annotation.FluxReference;
import com.swak.annotation.Get;
import com.swak.annotation.RestApi;
import com.swak.entity.Result;
import com.tmt.api.service.AsyncService;

/**
 * 异步
 * 
 * @author lifeng
 * @date 2020年4月29日 下午4:29:06
 */
@RestApi(path = "/api/async", value = "asyncApi")
public class AsyncApi {

	@FluxReference
	private AsyncService asyncService;

	/**
	 * 直接输出数据
	 * 
	 * @return
	 */
	@Get("/say")
	public CompletableFuture<Result> say() {
		return asyncService.asyncSay().thenApply(Result::success);
	}
}
