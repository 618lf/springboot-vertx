package com.tmt.api.web;

import java.util.concurrent.CompletableFuture;

import com.shop.ShopService;
import com.shop.ShopServiceAsync;
import com.swak.annotation.FluxReference;
import com.swak.annotation.Get;
import com.swak.annotation.RestService;
import com.swak.entity.Result;

/**
 * 定义一个服务
 * 
 * @author lifeng
 */
@RestService(path = "/api/shop")
public class ShopApi {

	@FluxReference
	private ShopService shopService;
	@FluxReference
	private ShopServiceAsync shopServiceAsync;

	@Get("/get")
	public Result get() {
		return Result.success(shopService.get());
	}

	@Get("/get/async")
	public CompletableFuture<Result> getAsync() {
		return shopServiceAsync.get().thenApply(Result.resultFunction());
	}
}