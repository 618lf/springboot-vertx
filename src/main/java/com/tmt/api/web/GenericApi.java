package com.tmt.api.web;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import com.swak.annotation.FluxReference;
import com.swak.annotation.Get;
import com.swak.annotation.RestApi;
import com.swak.entity.Result;
import com.swak.utils.Lists;
import com.tmt.api.entity.Fix;
import com.tmt.api.entity.FruitOrder;
import com.tmt.api.entity.Order;
import com.tmt.api.service.GenericServiceAsync;

/**
 * 泛型服务的调用
 * 
 * @author: lifeng
 * @Date: 2020/3/28 11:50
 */
@RestApi(path = "/api/generic", value = "genericApi")
public class GenericApi {

	@FluxReference
	GenericServiceAsync genericService;

	/**
	 * 主要测试泛型的方法的调用问题
	 */
	@Get("/say/b")
	public CompletableFuture<Result> sayB(FruitOrder order, List<Order> orders, Map<String, Object> params, Long id,
			Fix fix, List<Fix> fixs) {
		return genericService.B(order, orders, params, id, fix, fixs).thenCompose(res -> {
			return genericService.C(id, Lists.newArrayList(id));
		}).thenCompose(res -> {
			return genericService.A(order, orders, params, id, fix, fixs);
		}).thenApply(res -> {
			return Result.success();
		});
	}
}
