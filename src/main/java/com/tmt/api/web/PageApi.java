package com.tmt.api.web;

import java.util.concurrent.CompletableFuture;

import com.swak.annotation.Get;
import com.swak.annotation.RestPage;
import com.swak.entity.Model;

import io.vertx.core.http.Cookie;
import io.vertx.ext.web.RoutingContext;

/**
 * 页面
 * 
 * @author lifeng
 * @date 2020年4月25日 下午3:53:37
 */
@RestPage(path = "/page", value = "pageApi")
public class PageApi {

	/**
	 * 通过 模板输出数据
	 *
	 * @return html
	 */
	@Get("/index.html")
	public CompletableFuture<Model> html(RoutingContext context) {
		context.addCookie(Cookie.cookie("test", "123").setPath("/"));
		return CompletableFuture.completedFuture(Model.use("index.html").addAttribute("name", "lifeng"));
	}
}
