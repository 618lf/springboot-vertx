package com.tmt.api.web;

import com.swak.annotation.Counted;
import com.swak.annotation.Get;
import com.swak.annotation.RestApi;
import com.swak.annotation.Timed;
import com.swak.entity.Result;
import com.swak.security.Subject;

/**
 * 指标监控
 * 
 * @author 618lf
 */
@RestApi(path = "/api/metrics", value = "metricsApi")
public class MetricsApi {

	/**
	 * 方法级别的监控
	 *
	 * @param subject 用户
	 * @return 结果
	 */
	@Get("/get")
	@Timed
	@Counted
	public Result get(Subject subject) {
		return Result.success();
	}
}