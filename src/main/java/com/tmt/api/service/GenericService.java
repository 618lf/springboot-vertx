package com.tmt.api.service;

import java.util.List;

import com.swak.annotation.FluxAsync;
import com.swak.annotation.FluxService;
import com.tmt.api.entity.Fix;
import com.tmt.api.entity.Order;

/**
 * 创建服务，创建异步接口
 * 
 * @author lifeng
 * @date 2020年4月23日 上午9:41:34
 */
@FluxAsync
@FluxService
public class GenericService extends BaseService<Order, Long, List<Order>, Fix, List<Fix>> {

}