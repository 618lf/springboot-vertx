package com.tmt.api.service;

import com.swak.annotation.FluxAsync;

@FluxAsync
public interface InterfaceService {

	void get();
}
