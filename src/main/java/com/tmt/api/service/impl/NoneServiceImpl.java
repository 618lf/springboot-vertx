package com.tmt.api.service.impl;

import java.util.concurrent.CompletableFuture;

import org.springframework.stereotype.Service;

import com.tmt.api.service.NoneService;

/**
 * 演示没有发布成为服务
 * 
 * @author lifeng
 * @date 2020年9月28日 下午5:44:31
 */
@Service
public class NoneServiceImpl implements NoneService {

	/**
	 * 异步服务提供者
	 */
	@Override
	public CompletableFuture<Void> none() {
		System.out.println("服务调用到了！");
		return CompletableFuture.completedFuture(null);
	}
}