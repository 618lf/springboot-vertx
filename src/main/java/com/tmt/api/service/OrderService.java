package com.tmt.api.service;

import com.swak.annotation.FluxAsync;
import com.swak.entity.Page;
import com.swak.entity.Parameters;
import com.swak.persistence.QueryCondition;
import com.swak.service.BaseService;
import com.tmt.api.entity.Order;

/**
 * 订单服务
 * 
 * @author lifeng
 * @date 2020年4月19日 下午12:37:30
 */
@FluxAsync
public class OrderService extends BaseService<Order, Long> {

	@Override
	public Page page(QueryCondition query, Parameters param) {
		return null;
	}
}
