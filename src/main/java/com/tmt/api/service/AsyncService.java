package com.tmt.api.service;

import java.util.concurrent.CompletableFuture;

import com.swak.annotation.Context;
import com.swak.annotation.FluxService;

/**
 * 直接提供异步服务
 * 
 * @author lifeng
 * @date 2020年4月29日 下午4:27:19
 */
@FluxService(context = Context.IO)
public class AsyncService {

	/**
	 * 提供异步服务
	 * 
	 * @return
	 */
	public CompletableFuture<String> asyncSay() {
		return CompletableFuture.completedFuture("Hello!");
	}
}
