package com.tmt.api.service;

import com.swak.annotation.Get;
import com.swak.annotation.RestService;
import com.swak.entity.Result;

/**
 * 直接将API定位到服务
 * 
 * @author lifeng
 * @date 2020年4月29日 下午4:55:11
 */
@RestService(path = "/api/service")
public class MergeApiService {

	@Get("/say")
	public Result say() {
		return Result.success("Hello!");
	}
}
