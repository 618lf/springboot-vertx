package com.tmt.api.service;

import com.swak.annotation.FluxAsync;
import com.swak.annotation.FluxService;
import com.tmt.api.entity.User;
import com.tmt.api.entity.UserAccount;

@FluxAsync
@FluxService
public class UserService {

	public User get(Long id) {
		return new User();
	}

	public User asyncGet(Long id) {
		return new User();
	}

	public User getByAccount(String account) {
		return new User();
	}

	public void register(UserAccount account) {
	}
}