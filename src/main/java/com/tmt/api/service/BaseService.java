package com.tmt.api.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 一个比较复杂的泛型的例子
 * 
 * @author lifeng
 * @date 2020年4月23日 上午9:16:17
 */
public class BaseService<T, PK extends Serializable, Ts, FIX, FIXS>
		extends SuperService<PK, T, Ts, FIX, FIXS> {

	/**
	 * 测试泛型
	 * 
	 * @param entity
	 */
	public List<T> B(T entity, Ts entitys, Map<String, Object> params, PK id, FIX fix, FIXS fixs) {
		System.out.println("BaseService.B Invoke.");
		// int i = 1 / 0; // 模拟异常
		return null;
	}

	/**
	 * 测试泛型
	 * 
	 * @param entity
	 */
	public List<T> testGeneric(T entity, Map<String, Object> params, PK id) {
		System.out.println("BaseService.testGeneric Invoke.");
		return null;
	}
}
