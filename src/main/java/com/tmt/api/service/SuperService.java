package com.tmt.api.service;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * 最高层次的
 * 
 * @author lifeng
 * @date 2020年4月21日 下午11:31:20
 */
public class SuperService<PK extends Serializable, T, Ts, FIX, FIXS> extends SSuperService<PK> {

	/**
	 * 测试泛型
	 * 
	 * @param entity
	 */
	public List<T> A(T entity, Ts entitys, Map<String, Object> params, PK id, FIX fix, FIXS fixs) {
		System.out.println("SuperService.A Invoke.");
		return null;
	}
}
