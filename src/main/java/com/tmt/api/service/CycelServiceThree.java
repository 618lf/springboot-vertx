package com.tmt.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.swak.annotation.FluxService;

/**
 * 循环依赖服务3 -- 引用其他两个服务
 * 
 * @author lifeng
 * @date 2020年5月27日 下午6:09:32
 */
@FluxService
public class CycelServiceThree {

	/**
	 * 服务内部之前的依赖
	 */
	@Autowired
	private CycelServiceOne cycelServiceOne;
	/**
	 * 服务内部之前的依赖
	 */
	@Autowired
	private CycelServiceTwo cycelServiceTwo;

	/**
	 * 保存 -- 使用事务，让spring创建代理了
	 */
	@Transactional
	public void save() {
		cycelServiceOne.save();
		cycelServiceTwo.save();
	}
}
