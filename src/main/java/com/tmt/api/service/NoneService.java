package com.tmt.api.service;

import java.util.concurrent.CompletableFuture;

/**
 * 测试一个没有服务的例子
 * 
 * @author lifeng
 * @date 2020年9月28日 下午5:43:28
 */
public interface NoneService {

	/**
	 * 提供异步服务
	 * 
	 * @return
	 */
	CompletableFuture<Void> none();
}