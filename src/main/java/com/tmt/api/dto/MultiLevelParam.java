package com.tmt.api.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * 多层级的数据解析
 * param[level2][level3][level4][level5][name]=1
 * 
 * 如果使用列表则减少1层(每使用1次减少一层，列表占两层) 不支持解析到 level5 中的数据
 * param[items][0][level3][level4][name]=1
 * 
 * 最多支持两层的列表解析
 * param[items][0][items][0][name]=1
 * 
 * @author 618lf
 */
@Getter
@Setter
public class MultiLevelParam {

	private Level2 level2;
	private List<Level2> items;

	@Getter
	@Setter
	public static class Level2 {
		private Level3 level3;
		private List<Level3> items;
	}

	@Getter
	@Setter
	public static class Level3 {
		private Level4 level4;
	}

	@Getter
	@Setter
	public static class Level4 {
		private Level5 level5;
	}

	@Getter
	@Setter
	public static class Level5 {
		private String name;
	}

}
