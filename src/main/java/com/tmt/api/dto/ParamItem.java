package com.tmt.api.dto;

import java.util.List;

/**
 * @author: lifeng
 * @Date: 2020/3/28 11:50
 * @Description:
 */
public class ParamItem {

	private String name;
	private Integer size;
	private Param param;
	private List<ParamItemLast> lasts;

	public List<ParamItemLast> getLasts() {
		return lasts;
	}

	public void setLasts(List<ParamItemLast> lasts) {
		this.lasts = lasts;
	}

	public Param getParam() {
		return param;
	}

	public void setParam(Param param) {
		this.param = param;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	@Override
	public String toString() {
		return "name=" + name + ";" + "size=" + size + ";";
	}
}