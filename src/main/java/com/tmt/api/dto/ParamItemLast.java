package com.tmt.api.dto;

/**
 * 最后一层数据
 * 
 * @author 618lf
 */
public class ParamItemLast {

	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
