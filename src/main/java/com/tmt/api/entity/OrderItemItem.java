package com.tmt.api.entity;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderItemItem {
	List<OrderItemItemItem> items;
}
