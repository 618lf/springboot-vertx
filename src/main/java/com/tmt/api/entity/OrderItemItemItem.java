package com.tmt.api.entity;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderItemItemItem {

	String id;
	String name;
	List<OrderItemItemItem> items;
}
