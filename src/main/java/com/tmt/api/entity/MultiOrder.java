package com.tmt.api.entity;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MultiOrder {
	List<OrderItem> items;
}
