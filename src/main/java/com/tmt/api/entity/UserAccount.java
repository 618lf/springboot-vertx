package com.tmt.api.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * 用户账号信息
 * 
 * @author lifeng
 * @date 2020年4月14日 下午9:06:22
 */
@Getter
@Setter
public class UserAccount {

	/**
	 * username, openId, phone
	 */
	private String id;
	private String name;
	private String headimg;
	private String password;
	private Long userId;
}
