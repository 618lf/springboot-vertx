package com.tmt.api.entity;

import com.swak.annotation.Table;
import com.swak.entity.IdEntity;

import lombok.Getter;
import lombok.Setter;

/**
 * @author: lifeng
 * @Date: 2020/3/28 11:50
 * @Description:
 */
@Getter
@Setter
@Table("SYS_USER")
public class User extends IdEntity<User, Long> {

	private String name; // 管理员名称
	private String headimg; // 管理员图像
	private String openId; // 对UserAccount的冗余, 方便查询用户时带出其他信息
	private String userName;// 对UserAccount的冗余
	private String phone;// 对UserAccount的冗余

	// 登陆时使用的登陆方式
	private UserAccount account;
}
