package com.tmt.api.dao;

import org.springframework.stereotype.Repository;

import com.swak.persistence.BaseMapperImpl;
import com.tmt.api.entity.User;

@Repository
public class UserDao extends BaseMapperImpl<User, Long> {
}