package com.tmt.api.mapper;

import org.apache.ibatis.annotations.Mapper;

import com.swak.persistence.BaseMapper;
import com.tmt.api.entity.User;

@Mapper
public interface UserMapper extends BaseMapper<User, Long> {
}
