package com.tmt.api.service;

import com.tmt.api.entity.Fix;
import com.tmt.api.entity.Order;
import java.lang.Long;
import java.lang.Object;
import java.lang.String;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public interface GenericServiceAsync {
  CompletableFuture<List<Order>> B(Order entity, List<Order> entitys, Map<String, Object> params,
      Long id, Fix fix, List<Fix> fixs);

  CompletableFuture<List<Order>> testGeneric(Order entity, Map<String, Object> params, Long id);

  CompletableFuture<List<Order>> A(Order entity, List<Order> entitys, Map<String, Object> params,
      Long id, Fix fix, List<Fix> fixs);

  CompletableFuture<Long> C(Long t, List<Long> ts);
}
