package com.tmt.api.service;

import com.swak.entity.Page;
import com.swak.entity.Parameters;
import com.swak.persistence.QueryCondition;
import java.util.concurrent.CompletableFuture;

public interface OrderServiceAsync {
  CompletableFuture<Page> page(QueryCondition query, Parameters param);
}
