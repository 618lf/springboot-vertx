package com.tmt.api.service;

import com.tmt.api.entity.User;
import com.tmt.api.entity.UserAccount;
import java.lang.Long;
import java.lang.String;
import java.lang.Void;
import java.util.concurrent.CompletableFuture;

public interface UserServiceAsync {
  CompletableFuture<User> get(Long id);

  CompletableFuture<User> asyncGet(Long id);

  CompletableFuture<User> getByAccount(String account);

  CompletableFuture<Void> register(UserAccount account);
}
