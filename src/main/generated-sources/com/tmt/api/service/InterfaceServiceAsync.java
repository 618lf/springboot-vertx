package com.tmt.api.service;

import java.lang.Void;
import java.util.concurrent.CompletableFuture;

public interface InterfaceServiceAsync {
  CompletableFuture<Void> get();
}
