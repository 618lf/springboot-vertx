package com.shop;

import java.lang.String;
import java.util.concurrent.CompletableFuture;

public interface ShopServiceAsync {
  CompletableFuture<String> get();
}
