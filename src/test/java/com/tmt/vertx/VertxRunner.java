package com.tmt.vertx;

import java.util.concurrent.CountDownLatch;

import org.junit.Test;

import com.swak.annotation.FluxReference;
import com.tmt.AppRunnerTest;
import com.tmt.api.service.NoneService;
import com.tmt.api.service.UserServiceAsync;

/**
 * 启动测试
 * 
 * @author lifeng
 * @date 2020年9月28日 下午5:27:57
 */
public class VertxRunner extends AppRunnerTest {

	@FluxReference
	private UserServiceAsync userService;
	@FluxReference
	private NoneService noneService;

	@Test
	public void test() throws InterruptedException {
		userService.asyncGet(1L).whenComplete((r, e) -> {
			System.out.println(e);
		});
		System.out.println("--- 测试 1 ---");
		CountDownLatch latch = new CountDownLatch(1);
		latch.await();
	}

	@Test
	public void none() throws InterruptedException {
		noneService.none().whenComplete((r, e) -> {
			System.out.println("测试结束");
		});
		System.out.println("--- 测试2 ---");
		CountDownLatch latch = new CountDownLatch(1);
		latch.await();
	}
}
