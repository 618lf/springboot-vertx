package com.tmt.param;

import com.swak.http.HttpService;
import com.swak.utils.JsonMapper;
import com.swak.utils.Maps;
import com.tmt.AppRunnerTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * @author: lifeng
 * @Date: 2020/3/28 11:49
 * @Description:
 */
public class ParamTest extends AppRunnerTest {

	@Autowired
	private HttpService httpService;

	@Test
	public void testString() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		httpService.get().setUrl("http://127.0.0.1:8888/api/param/string").textResult().future()
				.whenComplete((res, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("Get参数：" + res);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void testStringId() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		httpService.get().setUrl("http://127.0.0.1:8888/api/param/string/123").textResult().future()
				.whenComplete((res, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("Path 参数：" + res);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void string_get_param() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		httpService.post().setUrl("http://127.0.0.1:8888/api/param/post_param").textResult().addFormParam("p1", "123")
				.addFormParam("p2", "1234").addFormParam("p3", "1").addFormParam("p3", "2").addFormParam("p3", "3")
				.future().whenComplete((res, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("Object对象封装：" + res);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void post_param_obj() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		httpService.post().setUrl("http://127.0.0.1:8888/api/param/post_param_obj").textResult()
				.addFormParam("p1", "123").addFormParam("p2", "1234").addFormParam("p3", "1").addFormParam("p3", "2")
				.addFormParam("p3", "3").addFormParam("p4[a]", "a").addFormParam("p4[b]", "b")
				.addFormParam("p4[c]", "c").addFormParam("oneItem[name]", "lifeng")
				.addFormParam("items[0][name]", "lifeng").addFormParam("items[1][name]", "hanqian").future()
				.whenComplete((res, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("Object对象封装，List参数：" + res);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void post_param_obj2() throws InterruptedException {
		Map<String, Object> p4 = Maps.newHashMap();
		p4.put("a", "a");
		p4.put("b", "b");
		p4.put("c", "c");
		CountDownLatch latch = new CountDownLatch(1);
		httpService.post().setUrl("http://127.0.0.1:8888/api/param/post_param_obj").textResult()
				.addFormParam("param[p1]", "123").addFormParam("param[p2]", "1234").addFormParam("param[p3]", "1")
				.addFormParam("param[p3]", "2").addFormParam("param[p3]", "3")
				.addFormParam("param[p4]", JsonMapper.toJson(p4)).future().whenComplete((res, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("Object对象封装，List参数：" + res);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void post_param_obj3() throws InterruptedException {
		Map<String, Object> p4 = Maps.newHashMap();
		p4.put("a", "a");
		p4.put("b", "b");
		p4.put("c", "c");
		CountDownLatch latch = new CountDownLatch(1);
		httpService.post().setUrl("http://127.0.0.1:8888/api/param/post_param_obj").textResult()
				.addFormParam("param[p1]", "123").addFormParam("param[p2]", "1234").addFormParam("param[p3][0]", "1")
				.addFormParam("param[p3][1]", "2").addFormParam("param[p3][2]", "3")
				.addFormParam("param[p4]", JsonMapper.toJson(p4)).future().whenComplete((res, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("Object对象封装，List参数：" + res);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void post_param_obj4() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		httpService.post().setUrl("http://127.0.0.1:8888/api/param/post_param_obj").textResult()
				.addFormParam("param[p1]", "123").addFormParam("param[p2]", "1234").addFormParam("param[p3][0]", "1")
				.addFormParam("param[p3][1]", "2").addFormParam("param[p3][2]", "3").addFormParam("param[p4][a]", "a")
				.addFormParam("param[p4][c]", "c").addFormParam("param[p4][b]", "b").future().whenComplete((res, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("Map对象封装：" + res);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void post_param_obj5() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		httpService.post().setUrl("http://127.0.0.1:8888/api/param/post_param_obj_mutil").textResult()
				.addFormParam("param[p1]", "123").addFormParam("param[p2]", "1234").addFormParam("param[p3][0]", "1")
				.addFormParam("param[p3][1]", "2").addFormParam("param[p3][2]", "3").addFormParam("param[p4][a]", "a")
				.addFormParam("param[p4][c]", "c").addFormParam("param[p4][b]", "b")
				.addFormParam("param[oneItem][name]", "lifeng").addFormParam("param[items][0][name]", "lifeng")
				.addFormParam("param[items][1][name]", "hanqian").future().whenComplete((res, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("二层对象参数封装：" + res);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void post_param_anno() throws InterruptedException {
		Map<String, Object> p4 = Maps.newHashMap();
		p4.put("a", "a");
		p4.put("b", "b");
		p4.put("c", "c");
		CountDownLatch latch = new CountDownLatch(1);
		httpService.post().setUrl("http://127.0.0.1:8888/api/param/post_param_anno").textResult()
				.addFormParam("json", JsonMapper.toJson(p4)).addHeader("name", "lifeng").future()
				.whenComplete((res, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("通过json传递参数：" + res);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void header() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		httpService.get().setUrl("http://127.0.0.1:8888/api/param/json").textResult().addHeader("name", "lifeng")
				.future().whenComplete((res, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("通过header传递参数：" + res);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void xml() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		httpService.get().setUrl("http://127.0.0.1:8888/api/param/xml").textResult().addHeader("name", "lifeng")
				.future().whenComplete((res, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("输出xml格式" + res);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void zerocopy() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		httpService.get().setUrl("http://127.0.0.1:8888/api/param/zerocopy").textResult().addHeader("name", "lifeng")
				.future().whenComplete((res, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("文件输出零拷贝：" + res.toString().length());
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void html() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		httpService.get().setUrl("http://127.0.0.1:8888/api/param/html").textResult().addHeader("name", "lifeng")
				.future().whenComplete((res, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("通过模板输出Html： " + res);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void post_param_valid() throws InterruptedException {
		Map<String, Object> p4 = Maps.newHashMap();
		p4.put("a", "a");
		p4.put("b", "b");
		p4.put("c", "c");
		CountDownLatch latch = new CountDownLatch(1);
		httpService.post().setUrl("http://127.0.0.1:8888/api/param/valid").textResult()
				.addFormParam("json", JsonMapper.toJson(p4)).addHeader("name", "lifeng").addFormParam("p1", "299")
				.addFormParam("p2", "swak-123").future().whenComplete((res, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("参数验证：" + res);
					latch.countDown();
				});
		latch.await();
	}
}