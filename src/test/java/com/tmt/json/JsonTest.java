package com.tmt.json;

import com.swak.utils.JsonMapper;
import com.swak.utils.Lists;
import com.tmt.api.entity.MultiOrder;
import com.tmt.api.entity.OrderItem;
import com.tmt.api.entity.OrderItemItem;
import com.tmt.api.entity.OrderItemItemItem;

public class JsonTest {

	public static void main(String[] args) {
		MultiOrder order = new MultiOrder();
		OrderItem orderItem = new OrderItem();
		OrderItemItem orderItemItem = new OrderItemItem();
		OrderItemItemItem orderItemItemItem = new OrderItemItemItem();
		OrderItemItemItem other = new OrderItemItemItem();
		orderItemItemItem.setId("1");
		orderItemItemItem.setName("2");
		other.setId("11");
		other.setName("22");
		order.setItems(Lists.newArrayList(orderItem));
		orderItem.setItems(Lists.newArrayList(orderItemItem));
		orderItemItem.setItems(Lists.newArrayList(orderItemItemItem));
		orderItemItemItem.setItems(Lists.newArrayList(other));
		String json = JsonMapper.toJson(order);
		System.out.println(json);
	}

}
