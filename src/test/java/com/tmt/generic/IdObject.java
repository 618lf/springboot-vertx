package com.tmt.generic;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class IdObject<PK> {

	protected PK id;

	public PK getId() {
		return id;
	}

	public void setId(PK id) {
		this.id = id;
	}

	/**
	 * 新增操作
	 *
	 * @return 主键
	 */
	public PK prePersist() {
		Type supperType = this.getClass().getGenericSuperclass();
		while (!(supperType instanceof ParameterizedType)) {
			supperType = ((Class<?>) supperType).getGenericSuperclass();
		}
		ParameterizedType parameterizedType = (ParameterizedType) supperType;
		Type actualtype = parameterizedType.getActualTypeArguments()[0];

		Class<?> pkType = null;
		if (actualtype instanceof Class) {
			pkType = (Class<?>) actualtype;
		}

		if (Long.class.isAssignableFrom(pkType)) {
			System.out.println("我是Long类型！");
		}
		if (String.class.isAssignableFrom(pkType)) {
			System.out.println("我是String类型！");
		}
		return this.getId();
	}
}
