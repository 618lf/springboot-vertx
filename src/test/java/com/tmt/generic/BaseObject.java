package com.tmt.generic;

public class BaseObject<PK> extends IdObject<PK> {
	protected String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
