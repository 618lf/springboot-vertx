package com.tmt.generic;

/**
 * 订单测试
 * 
 * @author lifeng
 * @date 2020年5月19日 下午5:20:42
 */
public class OrderTest {

	public static void main(String[] args) {

		Order order = new Order();
		order.prePersist();

		GoodsOrder goodsOrder = new GoodsOrder();
		goodsOrder.prePersist();

		StringOrder stringOrder = new StringOrder();
		stringOrder.prePersist();
	}
}
