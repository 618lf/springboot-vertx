package com.tmt.role;

import java.util.concurrent.CountDownLatch;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.swak.http.HttpService;
import com.swak.security.Principal;
import com.swak.security.Subject;
import com.swak.utils.Sets;
import com.swak.vertx.security.SecurityService;
import com.swak.vertx.security.SecuritySubject;
import com.tmt.AppRunnerTest;

/**
 * @author: lifeng
 * @Date: 2020/3/28 11:45
 * @Description:
 */
public class RoleTest extends AppRunnerTest {

	@Autowired
	private SecurityService jwt;
	@Autowired
	private HttpService httpService;

	@Test
	public void anno() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		httpService.get().setUrl("http://127.0.0.1:8888/api/anno/get").textResult().future().whenComplete((r, e) -> {
			if (e != null) {
				e.printStackTrace();
			}
			System.out.println("Anno: " + r);
			latch.countDown();
		});
		latch.await();
	}

	@Test
	public void Not_user() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		httpService.get().setUrl("http://127.0.0.1:8888/api/user/get").textResult().future().whenComplete((r, e) -> {
			if (e != null) {
				e.printStackTrace();
			}
			System.out.println("Not User: " + r);
			latch.countDown();
		});
		latch.await();
	}

	@Test
	public void Not_admin() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		httpService.get().setUrl("http://127.0.0.1:8888/api/admin/get").textResult().future().whenComplete((r, e) -> {
			if (e != null) {
				e.printStackTrace();
			}
			System.out.println("Not Admin: " + r);
			latch.countDown();
		});
		latch.await();
	}

	@Test
	public void user() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		Subject subject = new SecuritySubject();
		subject.setPrincipal(new Principal(1L, "李锋"));
		String token = jwt.generateToken(subject.toPayload());
		httpService.get().setHeader("X-token", token).setUrl("http://127.0.0.1:8888/api/user/get").textResult().future()
				.whenComplete((r, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("User: " + r);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void NO_admin_role() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		Subject subject = new SecuritySubject();
		subject.setPrincipal(new Principal(1L, "李锋"));
		String token = jwt.generateToken(subject.toPayload());
		httpService.get().setHeader("X-token", token).setUrl("http://127.0.0.1:8888/api/admin/get").textResult()
				.future().whenComplete((r, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("NO_admin_role: " + r);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void has_admin_role() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		Subject subject = new SecuritySubject();
		subject.setPrincipal(new Principal(1L, "李锋"));
		subject.setRoles(Sets.newHashSet("admin"));
		String token = jwt.generateToken(subject.toPayload());
		httpService.get().setHeader("X-token", token).setUrl("http://127.0.0.1:8888/api/admin/get").textResult()
				.future().whenComplete((r, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("has_admin_role: " + r);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void has_admin_role_no_roles() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		Subject subject = new SecuritySubject();
		subject.setPrincipal(new Principal(1L, "李锋"));
		subject.setRoles(Sets.newHashSet("admin", "admin:role1"));
		String token = jwt.generateToken(subject.toPayload());
		httpService.get().setHeader("X-token", token).setUrl("http://127.0.0.1:8888/api/admin/get_role").textResult()
				.future().whenComplete((r, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("has_admin_role_no_roles: " + r);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void has_admin_role_has_roles() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		Subject subject = new SecuritySubject();
		subject.setPrincipal(new Principal(1L, "李锋"));
		subject.setRoles(Sets.newHashSet("admin", "admin:role1", "admin:role2"));
		String token = jwt.generateToken(subject.toPayload());
		httpService.get().setHeader("X-token", token).setUrl("http://127.0.0.1:8888/api/admin/get_role").textResult()
				.future().whenComplete((r, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("has_admin_role_has_roles: " + r);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void has_admin_role_no_permissions() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		Subject subject = new SecuritySubject();
		subject.setPrincipal(new Principal(1L, "李锋"));
		subject.setRoles(Sets.newHashSet("admin"));
		subject.setPermissions(Sets.newHashSet("admin:role1:op1", "admin:role2:op2"));
		String token = jwt.generateToken(subject.toPayload());
		httpService.get().setHeader("X-token", token).setUrl("http://127.0.0.1:8888/api/admin/get_permission")
				.textResult().future().whenComplete((r, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("has_admin_role_no_permissions: " + r);
					latch.countDown();
				});
		latch.await();
	}

	@Test
	public void has_admin_role_has_permissions() throws InterruptedException {
		CountDownLatch latch = new CountDownLatch(1);
		Subject subject = new SecuritySubject();
		subject.setPrincipal(new Principal(1L, "李锋"));
		subject.setRoles(Sets.newHashSet("admin"));
		subject.setPermissions(Sets.newHashSet("admin:role1:op1", "admin:role2:op2"));
		String token = jwt.generateToken(subject.toPayload());
		httpService.get().setHeader("X-token", token).setUrl("http://127.0.0.1:8888/api/admin/get_permission")
				.textResult().future().whenComplete((r, e) -> {
					if (e != null) {
						e.printStackTrace();
					}
					System.out.println("has_admin_role_has_permissions: " + r);
					latch.countDown();
				});
		latch.await();
	}
}