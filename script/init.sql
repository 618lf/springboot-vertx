create table cloud_user(
    id          bigint,
    name        varchar,
    headimg     varchar
);

create table cloud_user_account(
    id          varchar,
    name        varchar,
    headimg     varchar,
    password    varchar,
    user_id     bigint
);